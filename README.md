# LyX hangulize

## About

[LyX](https://www.lyx.org/) is a [WYSIWYM](https://en.wikipedia.org/wiki/WYSIWYM) document processing program based on [LaTeX](https://en.wikipedia.org/wiki/LaTeX). And it is available with several languages including Chinese, Japanese, and Korean. However, its Korean language support is not satisfactory to the Korean LyX and/or LaTeX users nowadays.

This small package tries to fill the gap between LyX 2.4 and latest Korean LaTeX environment, with providing Unicode-based *ko.TeX* support.

## Version
* 2024/06/04: LyX 2.4-compatible

## Files

language file

* `languages`: Language definition file in `Resources` folder. We uncommented the unactivated setting of 'korean-kotex', and add 'korean-polyglossia', both of which are UTF-8.

layout files

* `kotex-article.layout`: article class for pdfTeX, XeTeX, or LuaTeX
* `oblivoir-article.layout`: Korean document class `oblivoir` in `article` mode with using XeTeX, LuaTeX.
* `oblivoir-book.layout`: Korean document class `oblivoir` in `memoir` mode with using XeTeX, LuaTeX.
* `kotex.module`: put `\usepackage{kotex}` in the preamble.
* `*.inc`: definition files for inclusion.

template files

* `oblivoir-art-template.lyx` and others

## Installation and Usage

Place all the files at their proper positions, and *reconfigure* LyX (Open the Menu, and find `Tools -> Reconfigure` item). Please read the documentation [`howtolyxhangulize.pdf`](https://bitbucket.org/novadh/lyx-hangulize/src/master/howtolyxhangulize.pdf) (in Korean).

## Copyright and License

* Copyright (c) 2020-2024 Nova de Hi
* MIT license





