# ko.TEX \dotemph definition
# Author : Nova de Hi

Format 66

InsetLayout Flex:DotEmph
	LyxType		custom
	LatexType	Command
	LatexName	dotemph
	Argument	1
		LabelString	"DotEmph"
		MenuString	"DotEmph"
		Tooltip	"Dotted Hangul Chars Emph"
	EndArgument
End
