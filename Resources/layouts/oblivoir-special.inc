# oblivoir special definition
# Author : Nova de Hi

Format 66

Input kotex-dotemph.inc

ModifyStyle Enumerate
        Argument 1
                LabelString   "Enumerate Options"
                Tooltip       "Optional arguments for this list"
        EndArgument
End

Style BoxedVerbatim
        CopyStyle             Verbatim
        LatexName             boxedverbatim
End

InsetLayout Flex:BNM
        LyxType         custom
        LatexType       Command
        LatexName       bnm
        Argument        1
                LabelString     "paren b"
                MenuString      "BNM"
                Tooltip "oblivoir's bnm paren"
        EndArgument
End

InsetLayout Flex:SNM
        LyxType         custom
        LatexType       Command
        LatexName       snm
        Argument        1
                LabelString     "paren s"
                MenuString      "SNM"
                Tooltip "oblivoir's snm paren"
        EndArgument
End


InsetLayout Flex:CNM
        LyxType         custom
        LatexType       Command
        LatexName       cnm
        Argument        1
                LabelString     "paren c"
                MenuString      "CNM"
                Tooltip "oblivoir's cnm paren"
        EndArgument
End

InsetLayout Flex:CCNM
        LyxType Custom
        LatexType       Command
        LatexName       ccnm
        Argument        1
                LabelString     "paren cc"
                MenuString      "CCNM"
                Tooltip "oblivoir's ccnm paren"
        EndArgument
End

